def my_agent(obs, config):
    # config.columns ... Number of Columns on the Board.
    # config.rows ... Number of Rows on the Board.
    # obs.board ... The current serialized Board (rows x columns).
    # config.inarow  ... Number of Checkers "in a row" needed to win.
    # obs.mark ... # Which player the agent is playing as (1 or 2).
     
    # Return which column to drop a checker (action).
    agent = PPO.load("ppo_model_vs_itself2")
    col, _ = model.predict(np.array(obs['board']).reshape(1,6,7))
    # Check if selected column is valid
    is_valid = (obs['board'][int(col)] == 0)
    # If not valid, select random move. 
    if is_valid:
        return int(col)
    else:
        return random.choice([col for col in range(config.columns) if obs.board[int(col)] == 0])