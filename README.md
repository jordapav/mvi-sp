# MVI



## Screenshot z kvízu

 [Kvíz](https://gitlab.fit.cvut.cz/jordapav/mvi-sp/-/blob/master/MVI-kviz.png)

## Semestrílní práce

Kaggle: Hra ConnectX [Odkaz na zadání](https://www.kaggle.com/competitions/connectx)

Jedná se o hru, ve které se hráč snaží získat určitý počet kamenů v řadě vodorovně, svisle nebo diagonálně, než se to podaří soupeřovi. Kameny se "pouštějí" z horní části jednoho sloupce hrací plochy. Hráči se střídají po 1 tahu.

Hodnocení programu probíhá hraním her proti programům, co napsali jiní soutěžící.


### Přečtené články / odkazy

Základní informace o hře, možnosti hraní, historie řešení: 
https://en.wikipedia.org/wiki/Connect_Four

Analýza hry, strategie hry:
Allen, J. D. "Expert Play in Connect-Four." https://tromp.github.io/c4.html

Reinforced learning na příkladu piškvorek:
https://towardsdatascience.com/reinforcement-learning-implement-tictactoe-189582bea542

Alexis Cook "Intro to Game AI and Reinforcement Learning" https://www.kaggle.com/code/alexisbcook/play-the-game


### Můj postup

V první verzi jsem si zatím připravil základní funkce, které bych mohl potřebovat a vyzkoušel jsem si vytvořit agenta, který hraje náhodně, ale pokud je v situaci kdy může dalším tahem vyhrát vybere ten výherní. (random_help.py)

Jako další jsem si vyzkoušel algoritmus, který předvídá 3 kroky dopředu. (3-step_lookahead.py)

V následném algoritmu si vyzkouším použití reinforced learning.

### Reinforced learning

Používám konvoluční neuronovou síť, která určuje pravděpodobnost výběru každého sloupce. Používám PPO (Proximal Policy Optimization) z knihovny Stable-Baselines3.

Odměny jsou +1 za výhru, -1 za prohru, -10 za neplatný tah a +1/(rows*columns) za jakýkoliv jiný tah, pro zlepšení konvergence.

#### Trénování 

Trénink probíhá hraním proti náhodnému agentovi, negamax a nakonec sám proti sobě.


### Výstupy (skóre na Kaggle)

[Odkaz na Kaggle leaderboard](https://www.kaggle.com/competitions/connectx/leaderboard?search=Pavel+Jord%C3%A1n)

Random algoritmus s pomocí: 177,2 (hodnota z 1.12.2022 21:49)

3-step lookahead: 730,7 (hodnota z 30.12.2022 23:10)

reinforced learning: nepodařilo se nahrát do soutěže, v offline testování vycházel horší něž 3-step lookahead (60% loss rate)

### Popis souborů

ppo_model_vs_xxx - vytrénované modely z knihovny stable-baselines3 PPO. (Prvně proti random, následně negamax a nakonec proti sobě.)

submission_reinforced.zip - nahrané do soutěže.

connectx_notebook2.ipynb - Notebook, s veškerým kódem pro reinforced learning, ve kterém se cvičil model.



